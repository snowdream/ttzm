---
layout: page
title: 丽江，香格里拉，梅里雪山
tagline: 天堂蜜月之行
comments: false
---
{% include JB/setup %}
  
   
   

&nbsp; &nbsp; &nbsp; &nbsp;从小到大，丽江，香格里拉，泸沽湖...这些地处云南的美丽地方，就一直让我很是向往。也曾幻想着像那些背包客一样背着背包，拄着登山杖去一一探索，却从未踏出一步。也曾想参加旅游团去这些地方旅行，却终究被充斥报端的各种旅游乱象和高昂的团费所吓倒。慢慢的，习惯了在遗憾中保持现状。  
&nbsp; &nbsp; &nbsp; &nbsp;直到去年年底结婚，老婆说想去丽江度蜜月，我毫不犹豫就答应了,可终因各种原因未能成行。到了今年六七月份的时候，一来工作开始没那么忙了，二来婚假都快过期了（注：婚假必须在结婚半年内请，否则过期无效），终于，下定决心，去旅行。
<br/>
<br/>
&nbsp; &nbsp; &nbsp; &nbsp;关于此次旅行的照片，请点击：[丽江，香格里拉，梅里雪山](http://user.qzone.qq.com/273247606/photo/V13Qfxqu0BtjSe/)

<hr/>
##前期准备
请假     
我提前一个月请婚假，假期8月12日到8月21日。加上周末8月10~11日，一共有假期12天。在这里要感谢领导，假期很快就得到批准；感谢同事，帮我分担着工作。

[more][1] 
<hr/>

##Day0 （8月10日）
8:20左右，在 友谊宾馆（北门航空售票处）乘坐机场快线，约半个小时后到达 首都国际机场。
 
[more][2] 
<hr/>

##Day1 （8月11日）
早点7点，到达丽江火车站。 
  
[more][3] 
<hr/>

##Day2 （8月12日）
早上8：00左右，我们在古城派出所等领队。8：30左右，我们登上了领队的商务车，出发了。

[more][4] 
<hr/>

##Day3 （8月13日）
早上8:00左右,出发去松赞林寺景区。9点左右，我们到达松赞林寺景区。  
从网上了解到，噶丹·松赞林寺是云南省规模最大的藏传佛教寺院，也是康区有名的大寺院之一，还是川滇一带的黄教中心，在整个藏区都有着举足轻重的地位，被誉为“小布达拉宫”。该寺依山而建，外形犹如一座古堡，集藏族造型艺术之大成，又有“藏族艺术博物馆”之称。

[more][5] 
<hr/>

##Day4 （8月14日）
清晨6:00，天还是黑的，有点微凉，我和媳妇都穿上了冲锋衣，跟着大家的脚步赶到观景台。在那里已经有一些游客架好了各种长枪短炮，准备迎接日出了。

[more][6] 
<hr/>

##Day5 （8月15日）
6:00左右，我们起来洗漱，收拾行李。在附近商店买了两瓶矿泉水，两瓶小酸奶，共11块钱。然后，我们回到饭馆吃早餐。早餐是每人一碗米线，味道很不错。

[more][7] 
<hr/>

##Day6 （8月16日）
9:30左右，睡觉睡到自然醒。洗漱，收拾后，我们就准备去束河古镇和其他几个团友会合了。  

[more][8] 
<hr/>

##Day7 （8月17日）
10:30左右，睡觉睡到自然醒。洗漱，收拾后，我们就在五一街附近逛了会。

[more][9] 
<hr/>

##Day811 （8月18日 ~ 8月21日）
8月18日
由于昨天晚上没睡好，今天11点多才起来。收拾行李，随便吃了点东西，我们就去客栈退了房子。

[more][10] 
<hr/>

##总结
这次旅行虽不完美，但很精彩。     

[more][11] 
<hr/>


[1]:pages/day-1.html
[2]:pages/day0.html
[3]:pages/day1.html
[4]:pages/day2.html
[5]:pages/day3.html
[6]:pages/day4.html
[7]:pages/day5.html
[8]:pages/day6.html
[9]:pages/day7.html
[10]:pages/day811.html
[11]:pages/day+1.html
