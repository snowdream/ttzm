---
layout: page
title: "前期准备"
description: "前期准备"
category : 天堂之蜜
tags : [北京]
comments: true
---
{% include JB/setup %}

##请假
&nbsp; &nbsp; &nbsp; &nbsp;我提前一个月请婚假，假期8月12日到8月21日。加上周末8月10~11日，一共有假期12天。在这里要感谢领导，假期很快就得到批准；感谢同事，帮我分担着工作。

##制定行程
由于我和媳妇都是菜驴，不能完全自助旅行，又怕参加旅游团会被迫进购物店，因此，经过我和媳妇几天的讨论，根据经济状况以及实际情况，我们制定了以下行程路线。
####Day0: 从北京飞昆明，在昆明乘坐晚上的火车去丽江
####Day1: 从丽江火车站去客栈，安排妥当后，去户外俱乐部签合同
####Day2-Day5: 参加“香格里拉梅里明永冰川纯玩四日”组团线路活动
####Day6-Day7: 丽江自由行
####Day8-Day11: 从丽江乘坐火车，经过昆明，返回北京
<br/>
<br/>
##机票，火车票，酒店，户外旅行团预定
机票和酒店是在去哪儿网上预定的。其中，机票的预定很顺利，但是酒店预定从一开始就出现了一点问题，主要问题出在去哪儿网 与商家的沟通不足上。
酒店选择了丽江的云起丽江客栈。朋友推荐的，环境不错，性价比还比较高。

户外旅行团活动是参加了8264论坛里狼鹰户外俱乐部的一个活动：
[心灵的朝圣 香格里拉梅里明永冰川纯玩四日 实体店！ 信誉保证！](http://bbs.8264.com/thread-1817899-1-1.html)
![IMG_1314.JPG](../imgs/IMG_1314.JPG "IMG_1314.JPG")      

火车票当然是在著名的12306网站预定的。可以到北京北站的取票机器上取出全程的火车票，并且不需要手续费，非常方便。（需要刷二代身份证）

##装备
一句话，装备基本靠攒。一切从零开始。  
###1.冲锋衣  
买了情侣装，两件套。合计：￥458.00元   
雷腾户外冲锋衣两件套含抓绒内胆 加厚保暖防雨抗寒 
[http://item.jd.com/1020917238.html](http://item.jd.com/1020917238.html)
![leiteng.jpg](../imgs/leiteng.jpg "雷腾户外冲锋衣两件套含抓绒内胆 加厚保暖防雨抗寒 ")
<br/>
<br/>
###2.登山包和登山杖
一个登山包，两根登山杖。合计：￥315.08元   
登山包有好有差，差的几十一个，好的上万一个。对于我这样的菜驴，经过对比，我选择了下面这款登山包。碰到是套装，和店主商量着，不要另外一个小包，补了点差价，拿了两根登山杖。   
拓峰 登山包 户外背包双肩 65+10升（背负系统升级版）    
[http://detail.taobao.com/meal_detail.htm?spm=a220o.1000855.0.0.ojeFXQ&meal_id=39646846&seller_id=1033264822](http://detail.taobao.com/meal_detail.htm?spm=a220o.1000855.0.0.ojeFXQ&meal_id=39646846&seller_id=1033264822)
![topfeng.gif](../imgs/topfeng.gif "拓峰 登山包 户外背包双肩 65+10升（背负系统升级版）")

没有要另外一个小包，补了点差价，拿了两根登山杖。  
拓峰 户外登山配件 可伸缩 弹簧避震登山杖拐杖 4节直柄T柄手杖      
[http://detail.tmall.com/item.htm?spm=a220o.1000855.1000800.d1.ojeFXQ&id=18049717426&bi_from=tm_comb](http://detail.tmall.com/item.htm?spm=a220o.1000855.1000800.d1.ojeFXQ&id=18049717426&bi_from=tm_comb)
<br/>
<br/>
###3.防晒衣
两件防晒衣。合计：79 + 65 = ￥144.00元  

男款  
WindTour 户外正品皮肤风衣 男女防紫外线速干超轻超薄透气防晒衣         
[http://trade.taobao.com/trade/detail/tradeSnap.htm?spm=a1z09.2.9.114.kMko6M&tradeID=392533754090860](http://trade.taobao.com/trade/detail/tradeSnap.htm?spm=a1z09.2.9.114.kMko6M&tradeID=392533754090860)  

女款  
正品1018户外男女情侣防紫外线皮肤风衣 超薄透明防晒风衣 速干衣                   
[http://detail.tmall.com/item.htm?id=19481151925&spm=a1z09.5.0.0.mdbhRe](http://detail.tmall.com/item.htm?id=19481151925&spm=a1z09.5.0.0.mdbhRe)
<br/>
<br/>
###4.太阳镜
两副太阳镜。合计：19.50 + 29.91= ￥49.41元  

男款  
偏光镜骑车眼镜女正品男士太阳镜蛤蟆镜防紫外线防风时尚司机墨镜             
[http://trade.taobao.com/trade/detail/tradeSnap.htm?spm=a1z09.2.9.24.kMko6M&tradeID=397587411330860](http://trade.taobao.com/trade/detail/tradeSnap.htm?spm=a1z09.2.9.24.kMko6M&tradeID=397587411330860)

女款  
包邮 防紫外线正品女太阳镜UB010F时尚渐变色墨镜眼镜 特价秒杀                        
[http://trade.taobao.com/trade/detail/tradeSnap.htm?spm=a1z09.2.9.8.kMko6M&tradeID=397559312060860](http://trade.taobao.com/trade/detail/tradeSnap.htm?spm=a1z09.2.9.8.kMko6M&tradeID=397559312060860)
<br/>
<br/>
##食品
我们在淘宝上买了一大堆的零食，全部装进登山包带走。
<br/>
<br/>
##药品
我们只买了一盒感冒药。合计：￥15.00元左右  
建议准备的药品如下：
抗高反药，感冒药，创可贴，晕车药 
   
