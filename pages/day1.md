---
layout: page
title: "在丽江"
description: "在丽江"
category : 天堂之蜜
tags : [丽江]
comments: true
---
{% include JB/setup %}

早点7点，到达丽江火车站。
![IMG_0181.JPG](../imgs/IMG_0181.JPG "IMG_0181.JPG")    
在丽江火车站，买了一份古城地图。乘坐4路公交车到达古城口，按照地图找到客栈-云起丽江客栈。  
![IMG_0205.JPG](../imgs/IMG_0205.JPG "IMG_0205.JPG")    
在客栈安排妥当，我们去狼鹰户外俱乐部和老板狼哥签了合同。之后，我和媳妇就在古城内大水车，四方街，五一街闲逛。晒几张照片先。  
![IMG_0222.JPG](../imgs/IMG_0222.JPG "IMG_0222.JPG")      

![IMG_0264.JPG](../imgs/IMG_0264.JPG "IMG_0264.JPG")      

![IMG_0258.JPG](../imgs/IMG_0258.JPG "IMG_0258.JPG")      

![IMG_0271.JPG](../imgs/IMG_0271.JPG "IMG_0271.JPG")      
